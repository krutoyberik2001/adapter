public class BirdAdapter implements LandAnimal {
    Bird bird;
    //Adapter
    public BirdAdapter(Bird bird){
        this.bird = bird;
    }
    @Override
    public void walk() {
        bird.fly();
    }
}
