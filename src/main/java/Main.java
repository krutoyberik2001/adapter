public class Main {
    public static void main(String[] args) {
        LandAnimal cat = new Cat();
        Bird eagle = new Eagle();

        System.out.print("Cat is: ");
        cat.walk();
        BirdAdapter eagleAdapter = new BirdAdapter(eagle);
        System.out.print("Eagle is:");
        eagleAdapter.walk();
        Bird pigeon=new Pigeon();
        BirdAdapter pigeonAdapter = new BirdAdapter(pigeon);
        System.out.print("Pigeon is: ");
        pigeonAdapter.walk();

    }
}
